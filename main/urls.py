from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('about/', views.about, name='about'),
    path('story1/', views.story1, name='story1'),
    path('isidata/', views.isidata, name='isidata'),
    path('savename/', views.savename, name = 'savename'),
    path('isidata/daftarkelas', views.daftarkelas, name = 'daftarkelas'),
    path('delete/<int:pk>', views.delete, name = 'delete'),
    path('details/<int:pk>', views.details, name = 'details'),
    path('activity/', views.activity, name = 'activity'),
    path('register/<int:pk>', views.register, name='register'),
    path('accordion/', views.accordion, name='accordion'),
    path('bookfinder/', views.bookfinder, name = 'bookfinder'),
    path('bookfinder/json/<str:searchKey>', views.json_response, name='json-story8-search'),
    path('user/', views.index9, name='landing'),
    path('user/login/', views.login9, name='login'),
    path('user/logout/', views.logout9, name='logout'),
    path('user/signup/', views.signup9, name='signup'),
]