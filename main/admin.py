from django.contrib import admin
from .models import Course, Person, Activity

# Register your models here.
admin.site.register(Course)
admin.site.register(Person)
admin.site.register(Activity)