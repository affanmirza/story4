from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, JsonResponse
from .forms import Input_Form
from .models import Course, Activity, Person
from django.urls import reverse 
from django.forms import inlineformset_factory, modelformset_factory
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
import requests

response = {}

def home(request):
    return render(request, 'homepage.html')

def about(request):
    return render(request, 'about.html')

def story1(request):
    return render(request, 'story1.html')

def isidata(request):
    response = {'input_form': Input_Form}
    return render(request, 'form.html', response)

def savename(request):
    form = Input_Form(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save() 
        return HttpResponseRedirect('/isidata/daftarkelas')
    else:
        return HttpResponseRedirect('/isidata/daftarkelas')

def daftarkelas(request):
	courses = Course.objects.all()
	response['courses'] = courses
	return render(request, 'results.html', response)

def delete(request, pk):
    kelas = Course.objects.filter(pk=pk)
    kelas.delete()    
    return HttpResponseRedirect('/isidata/daftarkelas')

def details(request, pk):
    kelas = Course.objects.get(pk=pk)
    response = {'kelas': kelas}
    return render(request, 'details.html', response)

def activity(request):
    activity_1 = Person.objects.filter(activity__name="Among Us")
    activity_2 = Person.objects.filter(activity__name="FIFA20")
    activity_3 = Person.objects.filter(activity__name="Nonton Film")
    response = {'activity_1': activity_1, 'activity_2': activity_2, 'activity_3': activity_3,}
    return render(request, 'activity.html', response)

def register(request, pk):
    PersonFormSet = inlineformset_factory(Activity, Person, fields=('name',))
    activity = Activity.objects.get(id=pk)
    formset = PersonFormSet(instance=activity)
    if request.method == "POST":
        formset = PersonFormSet(request.POST, instance=activity)
        if formset.is_valid():
            formset.save()
            return HttpResponseRedirect('/activity/')
    return render(request, 'register.html', {'formset': formset})

def accordion(request):
    return render(request, 'accordion.html')

def bookfinder(request):
    response = {
        
    }
    return render(request, 'bookfinder.html', response)


def json_response(request, **kwargs):
    resp = requests.get("https://www.googleapis.com/books/v1/volumes?q=" + kwargs["searchKey"])

    bookjson = resp.json()

    response = {
        "daftarBuku": [],
    }
    bookjson = bookjson["items"]
    
    for dataBuku in bookjson:
        buku = dataBuku["volumeInfo"]
        jsonBuku = {}


        jsonBuku["foto"] = buku["imageLinks"]["thumbnail"] if "imageLinks" in buku.keys() else "https://upload.wikimedia.org/wikipedia/commons/thumb/b/ba/No_image_available_400_x_600.svg/600px-No_image_available_400_x_600.svg.png"
        jsonBuku["judul"] = buku["title"] if "title" in buku.keys() else "Title Not Found"
        jsonBuku["deskripsi"] = buku["description"] if "description" in buku.keys() else "Description Not Found"

        response["daftarBuku"].append(jsonBuku)

    respon = JsonResponse(response)

    return respon

def index9(request):
    return render(request,'user.html')

def login9(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        print("{}: {} ".format(username,password))
        if not username or not password:
            messages.error(request, "Please enter username / password correctly")
            return HttpResponseRedirect('/user/login')

        user = authenticate(request,username = username,password = password)
        if user is not None:
            login(request,user)
            return HttpResponseRedirect('/user/')
        else:
            messages.error(request, "Wrong credentials (Wrong Username / Password)")
            return HttpResponseRedirect('/user/login')
    else:
        return render(request,'login.html')

def logout9(request):
    logout(request)
    messages.success(request, "Successfully logged out.")
    return HttpResponseRedirect('/user/')

def signup9(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        if not username or not password:
            messages.error(request, "Please enter username / password correctly")
            return HttpResponseRedirect('/user/signup')
        if User.objects.filter(username__iexact = username).exists():
            messages.error(request, "Username taken")
            return HttpResponseRedirect('/user/signup')
        else:
            user = User.objects.create_user(username=username, password=password)
            user.save()
            return HttpResponseRedirect('/user/login') #test for commit 1
    else:
        return render(request,'signup.html')