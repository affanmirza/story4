from django.db import models
from django.utils import timezone
from datetime import datetime, date

class Activity(models.Model):

    class Meta:
        verbose_name_plural = 'Activities'
    name = models.CharField(max_length=50)

class Course(models.Model):
	mata_kuliah = models.CharField(max_length=30)
	dosen = models.CharField(max_length=17)
	sks = models.CharField(max_length=3, default='none')
	deskripsi = models.CharField(max_length=140, default='none')
	semester_tahun = models.CharField(max_length=20, default='none')
	ruang_kelas = models.CharField(max_length=30, default='none')


class Person(models.Model):

    name = models.CharField(max_length=50)

    activity = models.ForeignKey(Activity, on_delete=models.CASCADE)