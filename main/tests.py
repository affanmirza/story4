from selenium import webdriver
from django.test import TestCase, LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from django.http import HttpRequest
from main.models import Course, Activity, Person
from main.forms import Input_Form
from main.views import *
import unittest


class ModelTest(TestCase):

    def setUp(self):
        self.mata_kuliah = Course.objects.create(mata_kuliah='ppw', dosen='bu ara',
        sks='3', deskripsi='lagi belajar unittest', semester_tahun='2020',
        ruang_kelas='rumah')

    def test_instance_created(self):
        self.assertEqual(Course.objects.count(), 1)

    def test_str(self):
        self.assertEqual(str(self.mata_kuliah.mata_kuliah), 'ppw')


class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.c = Course.objects.create(mata_kuliah='ppw', dosen='bu ara',
        sks='3', deskripsi='lagi belajar unittest', semester_tahun='2020', ruang_kelas='rumah')
        self.home = reverse('main:home')
        self.about= reverse('main:about')
        self.daftarkelas = reverse('main:daftarkelas')
        self.savename = reverse('main:savename')
        self.details = reverse('main:details', args=[self.c.id])
        self.delete = reverse('main:delete', args=[self.c.id])
        self.activity = reverse('main:activity')
        self.accordion = reverse('main:accordion')

    def test_GET_home(self):
        response = self.client.get(self.home)
        self.assertEqual(response.status_code, 200)

    def test_GET_about(self):
        response = self.client.get(self.about)
        self.assertEqual(response.status_code, 200)
    
    def test_GET_daftarkelas(self):
        response = self.client.get(self.daftarkelas)
        self.assertEqual(response.status_code, 200)

    def test_GET_savename(self):
        response = self.client.post(self.savename)
        self.assertEqual(response.status_code, 302)

    def test_GET_details(self):
        response = self.client.get(self.details)
        self.assertEqual(response.status_code, 200)

    def test_POST_details(self):
        response = self.client.post(self.details, {
            'mata_kuliah': 'ppw',
            'dosen': 'bu ara',
            'sks': '3',
            'deskripsi': 'i love ppw',
            'semester_tahun': '2020',
            'ruang_kelas': 'rumah',
        }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_GET_delete(self):
        response = self.client.get(self.delete)
        self.assertEqual(response.status_code, 302)

    def test_POST_delete(self):
        response = self.client.post(self.delete, {
            'mata_kuliah': 'ppw',
            'dosen': 'bu ara',
            'sks': '3',
            'deskripsi': 'i love ppw',
            'semester_tahun': '2020',
            'ruang_kelas': 'rumah',
        }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_GET_activity(self):
        self.activity_1 = Activity.objects.create(name='Among Us')
        self.activity_2 = Activity.objects.create(name='FIFA20')
        self.activity_3 = Activity.objects.create(name='Nonton Film')
        self.assertEqual(Activity.objects.count(), 3)
        response = self.client.get(self.activity)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'activity.html')

    def test_accordion_use_right_function(self):
        found = resolve(self.accordion)
        self.assertEqual(found.func, accordion)

    def test_GET_index(self):
        response = self.client.get(self.accordion)
        self.assertEqual(response.status_code, 200)


    def test_url(self):
        response = self.client.get("/bookfinder/")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'bookfinder.html')

    def test_searchBox(self):
        response = self.client.get("/bookfinder/json/Web Server")
        self.assertEqual(response.status_code, 200)

    def test_index_url(self):
        response = Client().get('/user/')
        self.assertEqual(response.status_code,200)

    def test_login_url(self):
        response = Client().get('/user/login/')
        self.assertEqual(response.status_code,200)
    
    def test_logout_url(self):
        response = Client().get('/user/logout/')
        self.assertEqual(response.status_code,302)

    def test_signup_url(self):
        response = Client().get('/user/signup/')
        self.assertEqual(response.status_code,200)

    def test_index_func(self):
        found = resolve('/user/')
        self.assertEqual(found.func, index9)

    def test_func_login(self):
        found = resolve('/user/login/')
        self.assertEqual(found.func,login9)

    def test_signup_func(self):
        found = resolve('/user/signup/')
        self.assertEqual(found.func,signup9)

    def test_templ_index(self):
        response = Client().get('/user/')
        self.assertTemplateUsed(response,'user.html')

    def test_templ_login(self):
        response = Client().get('/user/login/')
        self.assertTemplateUsed(response,'login.html')
    
    def test_templ_signup(self):
        response = Client().get('/user/signup/')
        self.assertTemplateUsed(response,'signup.html')

@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class Story4TestCase(TestCase):
    def test_root_url_status_200(self):
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('main:home'))
        self.assertEqual(response.status_code, 200)


class Story4FunctionalTestCase(FunctionalTestCase):
    def test_root_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())

    def test_root_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())
        