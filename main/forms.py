from django import forms
from .models import Course

class Input_Form(forms.ModelForm):
	class Meta:
		model = Course
		fields = ['mata_kuliah', 'dosen', 'sks', 'deskripsi', 'semester_tahun', 'ruang_kelas']


	error_messages = {
		'required' : 'Please Type'
	}

	matkul_attrs = {
		'type' : 'text',
		'placeholder' : 'Mata Kuliah'
	}

	dosen_attrs = {
		'type' : 'text',
		'placeholder' : 'Dosen'
	}

	sks_attrs = {
		'type' : 'text',
		'placeholder' : 'SKS'
	}

	deskripsi_attrs = {
		'type' : 'text',
		'placeholder' : 'Deskripsi'
	}

	semester_attrs = {
		'type' : 'text',
		'placeholder' : 'Semester'
	}

	kelas_attrs = {
		'type' : 'text',
		'placeholder' : 'No. Ruang Kelas'
	}


	mata_kuliah = forms.CharField(label='', required=True, max_length=27, widget=forms.TextInput(attrs=matkul_attrs))
	dosen = forms.CharField(label='', required=True, max_length=27, widget=forms.TextInput(attrs=dosen_attrs))
	sks = forms.CharField(label='', required=True, max_length=27, widget=forms.TextInput(attrs=sks_attrs))
	deskripsi = forms.CharField(label='', required=True, max_length=255, widget=forms.Textarea(attrs=deskripsi_attrs))
	semester_tahun = forms.CharField(label='', required=True, max_length=27, widget=forms.TextInput(attrs=semester_attrs))
	ruang_kelas = forms.CharField(label='', required=True, max_length=27, widget=forms.TextInput(attrs=kelas_attrs))