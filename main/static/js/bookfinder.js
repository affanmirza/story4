$(".my-searchbox").keyup(function (e) { 
    if(e.keyCode == 13){
        searchBar($("#search").val());
    }
});

function searchBar(str){
    $.ajax({
        dataType: "json",
        url: "json/" + str,
        success: function (response) {
            var listBuku = response["daftarBuku"];
            var tbody = $(".my-tablebody");
            tbody.empty();
            
            
            for(i = 0 ; i < listBuku.length ; i++){
                tbody.append("<tr>");
                var judul =  listBuku[i].judul;
                var linkFoto = listBuku[i].foto;
                var deskripsi = listBuku[i].deskripsi;

                tbody.append("<th scope='row'>"+ (i+1) +"</th>");
                tbody.append("<td>" + "<img class='img-fluid' src=" + linkFoto +">" + "</td>")
                tbody.append("<td>" + judul + "</td>" + "<td>" + deskripsi + "</td>");
                tbody.append("</tr>");
            }
        }
    });
}