$(".title").click(function () {
    $(this).parent().next().slideToggle();
})

$(".up").click(function () {
    const current = $(this).parent().parent().parent()
    const currentClass = 'accordion-' + current.css('order')
    const previousClass = 'accordion-' + parseInt(current.css('order') - 1)
    if (parseInt(current.css('order')) > 1) {
        $('.' + previousClass).addClass(currentClass)
        $('.' + previousClass).removeClass(previousClass)
        $(this).parent().parent().parent().removeClass(currentClass)
        $(this).parent().parent().parent().addClass(previousClass)
    }
})

$(".down").click(function () {
    const current = $(this).parent().parent().parent()
    const currentClass = 'accordion-' + current.css('order')
    const nextClass = 'accordion-' + parseInt(parseInt(current.css('order')) + 1)
    if (parseInt(current.css('order')) < 5) {
        $('.' + nextClass).addClass(currentClass)
        $('.' + nextClass).removeClass(nextClass)
        $(this).parent().parent().parent().removeClass(currentClass)
        $(this).parent().parent().parent().addClass(nextClass)
    }
})